import { SIGNED_IN, SET_COMPLETED } from "../constants";

let user = {
  email: null
};

export default (state = user, action) => {
  switch (action.type) {
    case SIGNED_IN:
      const { email } = action;
      user = {
        email
      };
      return user;
    case SET_COMPLETED:
    default:
      return state;
  }
};
