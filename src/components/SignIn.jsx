import React, { Component } from "react";
import { Link } from "react-router-dom";
import { firebaseApp } from "../firebase";
import { Redirect, withRouter } from "react-router-dom";
import { auth } from "../index";

class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      alertClass: "",
      error: {
        message: ""
      }
    };
  }

  signIn() {
    const { email, password } = this.state;
    firebaseApp
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(user => {
        this.setState({ error: { message: "" } });
        auth.authenticate();
        this.props.history.push("/");
      })
      .catch(error => {
        this.setState({
          error,
          alertClass: "alert alert-danger"
        });
      });
  }

  render() {
    if (auth.isAuth) {
      return <Redirect to="/" />;
    }
    return (
      <div className="form-inline" style={{ margin: "5%" }}>
        <h2>Sign In</h2>
        <div className="form-group">
          <input
            className="form-control"
            type="text"
            style={{ marginRight: "5px" }}
            placeholder="email"
            onChange={event => this.setState({ email: event.target.value })}
          />
          <input
            className="form-control"
            type="password"
            style={{ marginRight: "5px" }}
            placeholder="password"
            onChange={event => this.setState({ password: event.target.value })}
          />
          <button
            className="btn btn-primary"
            type="button"
            onClick={() => this.signIn()}
          >
            Sign In
          </button>
        </div>
        <div className={this.state.alertClass}>{this.state.error.message}</div>
        <div>
          <Link to={"/signup"}>Sign up instead</Link>
        </div>
      </div>
    );
  }
}

export default withRouter(SignIn);
