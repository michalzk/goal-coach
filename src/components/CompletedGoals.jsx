import React, { Component } from "react";
import { connect } from "react-redux";
import { setCompleted } from "../actions";
import { completeGoalRef } from "../firebase";

class CompletedGoals extends Component {
  componentDidMount() {
    completeGoalRef.on("value", snap => {
      let completedGoals = [];
      snap.forEach(completedGoal => {
        const { email, title } = completedGoal.val();
        completedGoals.push({ email, title });
      });
      this.props.setCompleted(completedGoals);
    });
  }
  clearCompleted() {
    completeGoalRef.set([]);
  }
  render() {
    return (
      <div>
        {this.props.completeGoals.map((completeGoal, index) => {
          const { title, email } = completeGoal;
          return (
            <div key={index}>
              <strong>{title}</strong> completed by <em>{email}</em>
            </div>
          );
        })}
        <button
          className="btn btn-primary"
          onClick={() => this.clearCompleted()}
        >
          Clear All
        </button>
      </div>
    );
  }
}
function mapStateToProps(state) {
  const { completeGoals } = state;
  return {
    completeGoals
  };
}
export default connect(mapStateToProps, { setCompleted })(CompletedGoals);
