import React, { Component } from "react";
import { connect } from "react-redux";
import { goalRef } from "../firebase";

class AddGoal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: ""
    };
  }

  addGoal = () => {
    const title = this.state.title;
    const email = this.props.user.email;
    goalRef
      .push({ email, title })
      .then(user => {
        console.log("Done!", user);
      })
      .catch(error => {
        console.log("Error!", error);
      });
  };

  render() {
    return (
      <div className="form-inline">
        <div className="form-group">
          <input
            type="text"
            placeholder="Add a goal"
            className="form-control"
            style={{ marginRight: "5px" }}
            onChange={event => this.setState({ title: event.target.value })}
          />
          <button
            className="btn btn-success"
            type="button"
            onClick={() => this.addGoal()}
          >
            Submit!
          </button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { user } = state;
  return {
    user
  };
}

export default connect(mapStateToProps, null)(AddGoal);
