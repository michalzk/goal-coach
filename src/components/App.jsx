import React, { Component } from "react";
import { connect } from "react-redux";
import { firebaseApp, goalRef } from "../firebase";
import { Redirect, withRouter } from "react-router-dom";
import { auth } from "../index";
import AddGoal from "./AddGoal";
import GoalList from "./GoalList";
import CompletedGoals from "./CompletedGoals";

class App extends Component {
  constructor(props) {
    super(props);
  }
  signOut() {
    firebaseApp.auth().signOut();
    auth.signOut();
    this.props.history.push("/signin");
  }
  render() {
    if (!auth.isAuth) {
      return <Redirect to="/" />;
    }
    return (
      <div style={{ margin: "5px" }}>
        <h3>Goal Coach v 1.0</h3>
        <AddGoal />
        <hr />
        <h4>Goals</h4>
        <GoalList />
        <hr />
        <h4>Completed Goals</h4>
        <CompletedGoals />
        <hr />
        <div>
          <button className="btn btn-danger" onClick={() => this.signOut()}>
            Sign Out
          </button>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

export default connect(mapStateToProps, null)(withRouter(App));
