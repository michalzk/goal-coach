import React, { Component } from "react";
import { Link } from "react-router-dom";
import { firebaseApp } from "../firebase";
import { Redirect, withRouter } from "react-router-dom";
import { auth } from "../index";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      password_confirmation: "",
      alertClass: "",
      error: {
        message: ""
      },
      success: {
        message: ""
      }
    };
  }

  signUp() {
    if (auth.isAuth) {
      this.props.history.push("/");
    } else {
      if (this.state.password !== this.state.password_confirmation) {
        this.setState({
          error: {
            message: "Password and password confirmation doesn't match"
          },
          success: { message: "" },
          alertClass: "alert alert-danger"
        });
      } else {
        firebaseApp
          .auth()
          .createUserWithEmailAndPassword(this.state.email, this.state.password)
          .then(user => {
            this.setState({
              success: { message: "User created successfully!" },
              error: { message: "" },
              alertClass: "alert alert-success"
            });
            auth.authenticate();
            this.props.history.push("/");
          })
          .catch(error => {
            this.setState({
              error,
              success: { message: "" },
              alertClass: "alert alert-danger"
            });
          });
      }
    }
  }

  render() {
    if (auth.isAuth) {
      return <Redirect to="/" />;
    }
    return (
      <div className="form-inline" style={{ margin: "5%" }}>
        <h2>Sign Up</h2>
        <div className="form-group">
          <input
            className="form-control"
            type="text"
            style={{ marginRight: "5px" }}
            placeholder="email"
            onChange={event => this.setState({ email: event.target.value })}
          />
          <input
            className="form-control"
            type="password"
            style={{ marginRight: "5px" }}
            placeholder="password"
            onChange={event => this.setState({ password: event.target.value })}
          />
          <input
            className="form-control"
            type="password"
            style={{ marginRight: "5px" }}
            placeholder="password confirmation"
            onChange={event =>
              this.setState({ password_confirmation: event.target.value })
            }
          />
          <button
            className="btn btn-primary"
            type="button"
            onClick={() => this.signUp()}
          >
            Sign Up
          </button>
        </div>
        <div className={this.state.alertClass}>
          {this.state.error.message}
          {this.state.success.message}
        </div>
        <div>
          <Link to={"/signin"}>Already a user? Sign in instead</Link>
        </div>
      </div>
    );
  }
}

export default withRouter(SignUp);
