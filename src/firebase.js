import * as firebase from "firebase";

const config = {
  apiKey: "AIzaSyCel84GELnFLtioyi9_EiGzSpAPEZi5zTY",
  authDomain: "goalcoach-6da31.firebaseapp.com",
  databaseURL: "https://goalcoach-6da31.firebaseio.com/",
  projectId: "goalcoach-6da31",
  storageBucket: "goalcoach-6da31.appspot.com",
  messagingSenderId: "289315383568"
};

export const firebaseApp = firebase.initializeApp(config);
export const goalRef = firebase.database().ref("goals");
export const completeGoalRef = firebase.database().ref("completeGoals");
