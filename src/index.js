import React from "react";
import ReactDOM from "react-dom";
import { Route, BrowserRouter, Redirect } from "react-router-dom";
import { firebaseApp } from "./firebase";
import { createStore } from "redux";
import { Provider } from "react-redux";
import { logUser } from "./actions";
import reducer from "./reducers";

import App from "./components/App";
import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";

import "./index.css";

const store = createStore(reducer);

export const auth = {
  isAuth: false,
  authenticate() {
    this.isAuth = true;
  },
  signOut() {
    this.isAuth = false;
  }
};

firebaseApp.auth().onAuthStateChanged(user => {
  if (user) {
    auth.authenticate();
    const { email } = user;
    store.dispatch(logUser(email));
  } else {
    auth.signOut();
  }
});

const PrivateRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        auth.isAuth ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/signin", state: { from: props.location } }}
          />
        )
      }
    />
  );
};

const rootElement = document.getElementById("root");
setTimeout(() => {
  ReactDOM.render(
    <Provider store={store}>
      <BrowserRouter>
        <div>
          <PrivateRoute exact path="/" component={App} />
          <Route path="/signin" component={SignIn} />
          <Route path="/signup" component={SignUp} />
        </div>
      </BrowserRouter>
    </Provider>,
    rootElement
  );
}, 1000);
